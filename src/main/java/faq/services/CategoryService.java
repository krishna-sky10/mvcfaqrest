package faq.services;

import faq.models.Category;

import java.util.List;


public interface CategoryService {

    //READ
    public Category findCategoryById(Long categoryId);

    public List<Category> findAllCategories();

    //CREATE
    public Category saveCategory(Category category);

    //UPDATE
    public Category updateCategory(Category category);

    //DELETE
    public void deleteCategory(Long categoryId);

    boolean isCategoryExist(Category category);
}
