package faq.services;

import faq.models.Category;
import faq.models.Metadata;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public interface MetadataService {

    public String findStatus(String status);

    public Integer findNumberOfObjectsWritten();

    public Integer findNumberOfObjectsRead();

    public Metadata setCategoryMetaData(HttpStatus status, Integer numberOfObjectsWritten);

    public Metadata setQuestionMetaData(HttpStatus status, Integer numberOfObjectsWritten);
}
