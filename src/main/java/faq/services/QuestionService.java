package faq.services;

import faq.models.Category;
import faq.models.Question;

import java.util.List;


public interface QuestionService {

    //CREATE
    public Question saveQuestion(Question question);

    //READ
    public List<Question> findQuestionsByCategoryId(Long categoryId);

    public Category findCategoryOfQuestion(Long categoryId);

    public Question findQuestionById(Long questionId);

    //UPDATE
    public Question updateQuestion(Question question);

    //DELETE
    void deleteQuestion(Long questionId);
}
