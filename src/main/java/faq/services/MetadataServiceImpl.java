package faq.services;

import faq.models.Category;
import faq.models.Metadata;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MetadataServiceImpl implements MetadataService{


    protected Metadata metadata = new Metadata();

    @Override
    public String findStatus(String status) {
        return null;
    }

    @Override
    public Integer findNumberOfObjectsWritten() {
        return null;
    }

    @Override
    public Integer findNumberOfObjectsRead() {
        return null;
    }

    @Override
    public Metadata setCategoryMetaData(HttpStatus status, Integer numberOfObjects) {
        metadata.setStatus(status);
        //metadata.setNumberOfObjectsRead(numberOfObjectsRead);
        metadata.setNumberOfObjectsWritten(numberOfObjects);

        return metadata;
    }

    @Override
    public Metadata setQuestionMetaData(HttpStatus status, Integer numberOfObjects){
        metadata.setStatus(status);
        //metadata.setNumberOfObjectsRead(numberOfObjectsRead);
        metadata.setNumberOfObjectsWritten(numberOfObjects);

        return metadata;
    }


}
