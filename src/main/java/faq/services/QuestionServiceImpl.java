package faq.services;

import faq.models.Category;
import faq.models.Question;
import faq.repositories.CategoryRepository;
import faq.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//Class implementing the methods of interface QuestionService
@Service
public class QuestionServiceImpl implements QuestionService{

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public List<Question> findQuestionsByCategoryId(Long categoryId){
        List<Question> questions;
        questions = questionRepository.findQuestionStatementByCategoryCategoryId(categoryId);
        return questions;
    }

    @Override
    public Question saveQuestion(Question question){
        return questionRepository.save(question);
    }

    @Override
    public Category findCategoryOfQuestion(Long categoryId) {
        Category category;
        category = categoryRepository.findByCategoryId(categoryId);

        return category;
    }

    @Override
    public Question updateQuestion(Question question) {
        return questionRepository.save(question);
    }

    @Override
    public void deleteQuestion(Long questionId){
        questionRepository.delete(questionId);
        return;
    }

    @Override
    public Question findQuestionById(Long questionId) {
        Question question = questionRepository.findOne(questionId);
        return question;
    }
}

//private EntityManager entityManager;

   /* @Override
    public List<Question> findQuestionsByCategoryId(Long categoryId) {
        Query query = entityManager.createQuery( "Select question_statement from questions q where q.category_id= ?1" );
        query.setParameter( 1,categoryId);
        return new ArrayList<Question>( query.getResultList() );
    }
*/