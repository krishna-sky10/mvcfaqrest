package faq.services;


import faq.models.Category;
import faq.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//Class implementing the methods of interface CategoryService
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> findAllCategories(){
        return categoryRepository.findAll();
    }

    @Override
    public Category saveCategory(Category category){
        return categoryRepository.save(category);
    }

    @Override
    public Category updateCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public void deleteCategory(Long categoryId) {
        categoryRepository.delete(categoryId);
        return;
    }

    @Override
    public boolean isCategoryExist(Category category) {
        if(categoryRepository.exists(category.getCategoryId())){
            return true;
        }
        return false;
    }

    @Override
    public Category findCategoryById(Long categoryId) {
        return categoryRepository.findOne(categoryId);
    }
}
