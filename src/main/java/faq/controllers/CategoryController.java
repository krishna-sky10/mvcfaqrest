package faq.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import faq.models.Category;
import faq.models.Metadata;
import faq.services.CategoryService;
import faq.services.MetadataService;
import jdk.nashorn.internal.ir.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

//Handles route mapping to Create, Read, Update, Delete Categories
@RestController
@RequestMapping("/faq/categories")
public class CategoryController {

    public static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    CategoryService categoryService;

    @Autowired
    MetadataService metadataService;


    @GetMapping("")
    public ResponseEntity<List<Category>> getAllCategories(){
        logger.info("Getting all categories...");

        List<Category> category = categoryService.findAllCategories();

        Metadata metadata = metadataService.setCategoryMetaData(HttpStatus.OK,category.size());

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Metadata",metadata.toString());
        ResponseEntity<List<Category>> responseEntity = new ResponseEntity<List<Category>> (category, httpHeaders, HttpStatus.OK);

        return responseEntity;

    }

    @GetMapping("/{categoryId}")
    public @ResponseBody Category getCategory(@PathVariable("categoryId") Long categoryId){
        Category category = categoryService.findCategoryById(categoryId);

        logger.info("Getting category <{}>",category.getCategoryName());

        return categoryService.findCategoryById(categoryId);
    }

    @PostMapping("")
    public ResponseEntity<Category> createCategory(@Valid @RequestBody Category category) {

        logger.info("Creating category <{}>...", category.getCategoryName());

        Metadata metadata = metadataService.setCategoryMetaData(HttpStatus.CREATED,1);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Metadata",metadata.toString());

        categoryService.saveCategory(category);

        return new ResponseEntity<Category>(category, httpHeaders, HttpStatus.CREATED);
    }

    @PutMapping("/{categoryId}")
    public Category updateCategory(@Valid @RequestBody Category category,
                                   @PathVariable("categoryId") Long categoryId){

        Category prevCategory = categoryService.findCategoryById(categoryId);
        logger.info("Updating category <{}>...", category.getCategoryName());

        Category newCategory = new Category();
        newCategory.setCategoryId(prevCategory.getCategoryId());
        newCategory.setCategoryName(category.getCategoryName());

        return categoryService.updateCategory(newCategory);
    }

    @DeleteMapping("/{categoryId}")
    public void deleteCategory(@PathVariable("categoryId") Long categoryId){

        Category category = categoryService.findCategoryById(categoryId);
        logger.info("Deleting category <{}>...",category.getCategoryName());

        categoryService.deleteCategory(categoryId);

        return;
    }

}
