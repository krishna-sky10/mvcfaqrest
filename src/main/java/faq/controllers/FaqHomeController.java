package faq.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FaqHomeController {

    @RequestMapping({"/","/faq"})
    public String index() {
        return "index" ;
    }
}
