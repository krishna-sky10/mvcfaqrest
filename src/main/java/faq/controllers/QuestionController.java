package faq.controllers;

import faq.models.Category;
import faq.models.Metadata;
import faq.models.Question;
import faq.services.MetadataService;
import faq.services.QuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.net.ssl.HttpsURLConnection;
import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.Callable;

//Handles route mapping to Create, Read, Update, Delete Questions
@RestController
@RequestMapping("/faq/categories")
public class QuestionController {

    public static final Logger logger = LoggerFactory.getLogger(QuestionController.class);

    @Autowired
    QuestionService questionService;

    @Autowired
    MetadataService metadataService;
    //Route Mapping

    @GetMapping("/{categoryId}/questions")
    public ResponseEntity<List<Question>> getAllQuestions(@PathVariable("categoryId") Long categoryId){
        //return (List<Question>) questionRepository.findAll();
        Category category = questionService.findCategoryOfQuestion(categoryId);
        logger.info("Getting all questions for category <{}>...", category.getCategoryName());

        List<Question> questions = questionService.findQuestionsByCategoryId(categoryId);
        Metadata metadata = metadataService.setQuestionMetaData(HttpStatus.OK,questions.size());

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Metadata",metadata.toString());
        ResponseEntity<List<Question>> responseEntity = new ResponseEntity<List<Question>> (questions, httpHeaders, HttpStatus.OK);

        return responseEntity;
    }


    @GetMapping("/{categoryId}/questions/{questionId}")
    public ResponseEntity<?> getQuestionById(@PathVariable("categoryId") Long categoryId,
                                             @PathVariable("questionId") Long questionId){

        Question question = questionService.findQuestionById(questionId);
        if(question.getCategory().getCategoryId() != categoryId){
            logger.error("Question does not belong to this category,go to /"
                    + categoryId
                    + "/questions/"
            + questionId);

            HttpHeaders headers = new HttpHeaders();
            headers.set("Message","Question Not Found : Question does not belong to this category.");

            return new ResponseEntity<>(headers,HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Question>(question,HttpStatus.OK);
    }


    @PostMapping("/{categoryId}/questions")
    public Question createQuestion(@Valid @RequestBody Question question,
                                   @PathVariable(name = "categoryId") Long categoryId){

        Category category = questionService.findCategoryOfQuestion(categoryId);
        logger.info("Creating question <{}> in category <{}>...",question.getQuestionStatement(),category.getCategoryName());

        question.setCategory(category);

        return questionService.saveQuestion(question);
    }


    @PutMapping("/{categoryId}/questions/{questionId}")
    public Question updateQuestion(@Valid @RequestBody Question question,
                                   @PathVariable("categoryId") Long categoryId,
                                   @PathVariable("questionId") Long questionId){

        Question previousQuestion = questionService.findQuestionById(questionId);
        Category category = questionService.findCategoryOfQuestion(categoryId);
        logger.info("Updating question <{}> to <{}> in category <{}>...",previousQuestion.getQuestionStatement(),
                question.getQuestionStatement(),
                category.getCategoryName());

        Question newQuestion = new Question();
        newQuestion.setQuestionId(previousQuestion.getQuestionId());
        newQuestion.setQuestionStatement(question.getQuestionStatement());
        newQuestion.setAnswerStatement(question.getAnswerStatement());

        newQuestion.setCategory(category);

        return questionService.updateQuestion(newQuestion);

    }


    @DeleteMapping("/{categoryId}/questions/{questionId}")
    public void deleteQuestion(@PathVariable("categoryId") Long categoryId,
                               @PathVariable("questionId") Long questionId){

        Category category = questionService.findCategoryOfQuestion(categoryId);
        Question question = questionService.findQuestionById(questionId);
        logger.info("Deleting question <{}> in category <{}>...",question.getQuestionStatement(),category.getCategoryName());

        questionService.deleteQuestion(questionId);
    }
}
