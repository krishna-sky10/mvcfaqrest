package faq.repositories;

import faq.models.Category;
import faq.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface QuestionRepository extends CrudRepository<Question, Long> {

    //@Query("Select question_statement from questions q where q.category_id=?1", nativeQuery=true)
    List<Question> findQuestionStatementByCategoryCategoryId(Long categoryId);

}
