package faq.repositories;

import faq.models.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category, Long> {

    List<Category> findAll();

    Category findByCategoryId(Long categoryId);

}
