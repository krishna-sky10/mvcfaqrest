package faq.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import org.hibernate.validator.constraints.NotBlank;

import javax.naming.Context;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "categories")
//@JsonRootName(value = "category")
//@JsonIgnoreProperties("{categoryId}")
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
//@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)

public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //@JsonIgnore
    private Long categoryId;

    @NotBlank
    private String categoryName;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "category")
    private Set<Question> questions = new HashSet<Question>();

    public Category(){

    }

    public Category(Long categoryId){
        super();
        this.categoryId = categoryId;
        //this.questions = questions ;
    }

    public Category(Long categoryId, String categoryName){
        super();
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        //this.questions = questions ;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "Category : {" +
                "category_id:" + categoryId +
                ",category_name='" + categoryName + "'" + "}";
    }
}
