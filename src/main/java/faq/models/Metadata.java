package faq.models;


import org.springframework.http.HttpStatus;

public class Metadata {

    private HttpStatus status;

    private Integer numberOfObjectsWritten;

    private Integer numberOfObjectsRead;

    public Metadata(){

    }

    public Metadata(HttpStatus status, Integer numberOfObjectsRead, Integer numberOfObjectsWritten){

        this.status = status;
        this.numberOfObjectsRead = numberOfObjectsRead;

    }


    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public Integer getNumberOfObjectsWritten() {
        return numberOfObjectsWritten;
    }

    public void setNumberOfObjectsWritten(Integer numberOfObjectsWritten) {
        this.numberOfObjectsWritten = numberOfObjectsWritten;
    }

    public Integer getNumberOfObjectsRead() {
        return numberOfObjectsRead;
    }

    public void setNumberOfObjectsRead(Integer numberOfObjectsRead) {
        this.numberOfObjectsRead = numberOfObjectsRead;
    }

    @Override
    public String toString() {

        String meta = new String("");

        meta = "Status: " + this.getStatus()
                + "; Objects Written Or Read: " + this.getNumberOfObjectsWritten()
                + "; Objects Read: " + this.getNumberOfObjectsRead() ;

        return meta;
    }
}
